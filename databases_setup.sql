DROP DATABASE `symfony_blog` ;
CREATE SCHEMA `symfony_blog` ;
CREATE TABLE `symfony_blog`.`Categories` (
  `idCategories` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NULL,
  PRIMARY KEY (`idcategories`));
CREATE TABLE `symfony_blog`.`BlogPost` (
  `idBlogPost` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NULL,
  `content` TEXT NULL,
  `imgUrl` VARCHAR(200), 
  `author` VARCHAR(45) NULL,
  `creationDate` DATETIME NULL,
  `idCategories` INT NULL,
  PRIMARY KEY (`idBlogPost`),
  INDEX `fk_BlogPost_1_idx` (`idCategories` ASC),
  CONSTRAINT `fk_BlogPost_1`
    FOREIGN KEY (`idCategories`)
    REFERENCES `symfony_blog`.`Categories` (`idCategories`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    CREATE TABLE `symfony_blog`.`BlogComment` (
  `idBlogComment` INT NOT NULL AUTO_INCREMENT,
  `content` VARCHAR(500) NULL,
  `creationDate` DATETIME NULL,
  `author` VARCHAR(45) NULL,
  `idBlogPost` INT NULL,
  PRIMARY KEY (`idBlogComment`),
  INDEX `fk_BlogComment_1_idx` (`idBlogPost` ASC),
  CONSTRAINT `fk_BlogComment_1`
    FOREIGN KEY (`idBlogPost`)
    REFERENCES `symfony_blog`.`BlogPost` (`idBlogPost`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
INSERT INTO `symfony_blog`.`Categories` (`title`) VALUES ('Romans');
INSERT INTO `symfony_blog`.`Categories` (`title`) VALUES ('Sérieux');
INSERT INTO `symfony_blog`.`Categories` (`title`) VALUES ('Découverte');



