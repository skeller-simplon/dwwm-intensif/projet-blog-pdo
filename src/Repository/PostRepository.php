<?php

namespace App\Repository;

use App\Entity\BlogPost;


class PostRepository
{
    public function findAll(): array
    {
        $posts = [];
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("SELECT * FROM BlogPost");
        $query->execute();
        foreach ($query->fetchAll() as $key => $line) {
            $posts[] = $this->sqlToPost($line);
        }
        return $posts;
    }
    public function findById(int $id): ? BlogPost
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM BlogPost WHERE idBlogPost = :id");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();
        if ($result = $query->fetch()) {
            return $this->sqlToPost($result);
        }
        return null;
    }
    public function findAllByCategoryId(int $id): ? array
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM BlogPost WHERE idCategories = :idCategories");
        $query->bindValue(":idCategories", $id, \PDO::PARAM_INT);
        $query->execute();
        if ($result = $query->fetchAll()) {
            $tab = [];
            foreach ($result as $value) {
                $tab[] = $this->sqlToPost($value);
            }
            return $tab;
        }
        return null;
    }
    public function findByName(string $key): ? array
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM BlogPost WHERE title LIKE :toCheck");
        $query->bindValue(":toCheck", "%" . $key . "%", \PDO::PARAM_STR);
        $query->execute();
        if ($result = $query->fetchAll()) {
            $tab = [];
            foreach ($result as $value) {
                $tab[] = $this->sqlToPost($value);
            }
            return $tab;
        }
        return null;
    }
    public function findCategoryName(int $id): ? array
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT title FROM Categories WHERE idCategory = :id");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();
        return $query->fetch();
    }
    public function add(BlogPost $post): void
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("INSERT INTO BlogPost (title, content, author, creationDate, idCategories, imgUrl) 
                                        VALUES (:title, :content, :author, NOW(), :idCategories, :imgUrl)");
        $query->bindValue(":title", $post->title, \PDO::PARAM_STR);
        $query->bindValue(":content", $post->content, \PDO::PARAM_STR);
        $query->bindValue(":author", $post->author, \PDO::PARAM_STR);
        $query->bindValue(":idCategories", $post->idCategories, \PDO::PARAM_INT);
        $query->bindValue(":imgUrl", $post->imgUrl, \PDO::PARAM_STR);
        $query->execute();

        $post->idBlogPost = $connection->lastInsertId();
    }
    public function remove(BlogPost $post): void
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("DELETE FROM BlogPost WHERE idBlogPost=:id");
        $query->bindValue(":id", $post->idBlogPost, \PDO::PARAM_INT);
        $query->execute();
    }
    public function update(BlogPost $post)
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("UPDATE BlogPost 
                                        SET title = :title, content = :content, author = :author, lastModified = NOW(),
                                            idCategories = :idCategories, imgUrl = :imgUrl
                                        WHERE idBlogpost = :idBlogPost;");
        $query->bindValue(":idBlogPost", $post->idBlogPost, \PDO::PARAM_INT);
        $query->bindValue(":title", $post->title, \PDO::PARAM_STR);
        $query->bindValue(":content", $post->content, \PDO::PARAM_STR);
        $query->bindValue(":author", $post->author, \PDO::PARAM_STR);
        $query->bindValue(":idCategories", $post->idCategories, \PDO::PARAM_INT);
        $query->bindValue(":imgUrl", $post->imgUrl, \PDO::PARAM_STR);
        $query->execute();
    }
    private function sqlToPost(array $line): BlogPost
    {
        $post = new BlogPost;
        $post->idBlogPost = intval($line["idBlogPost"]);
        $post->title = $line["title"];
        $post->content = $line["content"];
        $post->author = $line["author"];
        $post->creationDate = $line["creationDate"];
        $post->idCategories = intval($line["idCategories"]);
        $post->imgUrl = $line["imgUrl"];
        $post->lastModified = $line["lastModified"];
        return $post;
    }
}
