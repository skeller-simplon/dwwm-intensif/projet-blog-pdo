<?php

namespace App\Repository;

use App\Entity\Categories;


class CategoriesRepository
{
    public function findAll(): array
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM Categories");
        $query->execute();
        foreach ($query->fetchAll() as $line) {
            $categories[] = $this->sqlToObject($line);
        }
        return $categories;

    }
    public function findById(int $id): ? Categories
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM Categories WHERE idCategory = :id");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();
        if ($result = $query->fetch()) {
            return $this->sqlToObject($result);
        }
        return null;
    }
    public function add(Categories $category): void
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("INSERT INTO Categories (title) VALUES (:title);");
        $query->bindValue(":title", $category->title, \PDO::PARAM_STR);
        $query->execute();
        $category->id = $connection->lastInsertId();

    }
    public function remove(Categories $category): void
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("DELETE FROM Categories WHERE idCategory=:id");
        $query->bindValue(":id", $category->id, \PDO::PARAM_INT);
        $query->execute();

    }
    public function update(Categories $category)
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("UPDATE Categories 
                                       SET title = :title 
                                       WHERE idCategory = :id");
        $query->bindValue(":title", $category->title, \PDO::PARAM_STR);
        $query->bindValue(":id", $category->id, \PDO::PARAM_INT);

    }
    private function sqlToObject(array $line): Categories
    {
        $category = new Categories;
        $category->title = $line["title"];
        $category->idCategory = intval($line["idCategory"]);
        return $category;
    }
}
