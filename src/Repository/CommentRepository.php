<?php

namespace App\Repository;

use App\Entity\BlogComment;


class CommentRepository
{

    public function findAllComments(): array
    {
        $posts = [];
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("SELECT * FROM BlogComment");
        $query->execute();
        foreach ($query->fetchAll() as $key => $line) {
            $posts[] = $this->sqlToComment($line);
        }
        return $posts;
    }
    public function findCommentById(int $id): ? BlogComment
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM BlogComment WHERE id = :id");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();
        if ($result = $query->fetch()) {
            return $this->sqlToComment($result);
        }
        return null;
    }
    public function findAllCommentsByPostId(int $id): ? array
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM BlogComment WHERE idBlogPost = :id");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();
        if ($result = $query->fetchAll()) {
            $commentaires = [];
            foreach ($result as $value) {
                $commentaires[] = $this->sqlToComment($value);
            }
            return $commentaires;
        }
        return null;
    }
    public function addComment(BlogComment $comment): void
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("INSERT INTO BlogComment (content, author, creationDate, idBlogPost) 
                                        VALUES (:content, :author, NOW(), :idBlogPost)");
        $query->bindValue(":content", $comment->content, \PDO::PARAM_STR);
        $query->bindValue(":author", $comment->author, \PDO::PARAM_STR);
        $query->bindValue(":idBlogPost", $comment->idBlogPost, \PDO::PARAM_INT);
        $query->execute();

        $comment->idBlogComment = $connection->lastInsertId();
    }
    public function removeComment(BlogComment $post): void
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("DELETE FROM BlogComment WHERE id=:id");
        $query->bindValue(":id", $post->id, \PDO::PARAM_INT);
        $query->execute();
    }
    public function updateComment(BlogComment $post)
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("UPDATE BlogComment 
                                        SET content=:content,author=:author, creationDate = :creationDate, idBlogPost = :idBlogPost
                                        WHERE id=:idBlogComment");
        $query->bindValue(":idBlogComment", $post->idBlogComment, \PDO::PARAM_INT);
        $query->bindValue(":content", $post->content, \PDO::PARAM_STR);
        $query->bindValue(":author", $post->author, \PDO::PARAM_STR);
        $query->bindValue(":creationDate", $post->creationDate, \PDO::PARAM_STR);
        $query->bindValue(":idBlogPost", $post->idBlogPost, \PDO::PARAM_INT);
        $query->execute();
    }
    private function sqlToComment(array $line): BlogComment
    {
        $post = new BlogComment;
        $post->idBlogComment = intval($line["idBlogComment"]);
        $post->content = $line["content"];
        $post->creationDate = $line["creationDate"];
        $post->author = $line["author"];
        $post->idBlogPost = intval($line["idBlogPost"]);
        return $post;
    }
}

