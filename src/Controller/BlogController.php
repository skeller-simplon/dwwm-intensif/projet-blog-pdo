<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\PostRepository;
use App\Entity\BlogPost;
use App\Form\BlogPostType;
use App\Form\EditBlogPostType;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\CommentRepository;

class BlogController extends AbstractController
{
    /**
     * @Route("/", name="blog_index")
     */
    public function index(PostRepository $postRepo)
    {
        $blogPosts = $postRepo->findAll();

        return $this->render("index.html.twig", [
            "blogPosts" => $blogPosts,
        ]);
    }
    /**
     * @Route ("/all-articles", name="all_articles")
     */
    public function allArticles(PostRepository $postRepo)
    {
        $blogPosts = $postRepo->findAll();

        return $this->render("all-articles.html.twig", [
            "blogPosts" => $blogPosts,
        ]);
    }
    /**
     * @Route("/article/{id}", name="display_article")
     */
    public function displayArticle(PostRepository $postRepo, int $id, CommentRepository $commentRepository)
    {
        $post = $postRepo->findById($id);
        $comments = $commentRepository->findAllCommentsByPostId($id);

        return $this->render("display-article.html.twig", [
            "post" => $post,
            "comments" => $comments
        ]);
    }
    /**
     * @Route("/admin/article/{id}/edit", name="edit_article")
     */
    public function editArticle(Request $request, int $id, PostRepository $postRepo)
    {
        $blogPost = $postRepo->findById($id);
        $form = $this->createForm(EditBlogPostType::class, $blogPost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $postRepo->update($blogPost);
            return $this->redirectToRoute("display_article", [
                'id' => $blogPost->idBlogPost
            ]);
        }
        return $this->render("edit-article.html.twig", [
            "post" => $blogPost,
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route ("/new-post", name="new_post")
     */
    public function postForm(PostRepository $postRepo, Request $request)
    {
        $blogPost = new BlogPost();
        $form = $this->createForm(BlogPostType::class, $blogPost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $postRepo->add($blogPost);
            return $this->redirectToRoute("display_article", [
                'id' => $blogPost->idBlogPost
            ]);
        }
        return $this->render("submitPost.html.twig", [
            "form" => $form->createView(),
        ]);
    }
    /**
     * @Route("/search", name="search_field");
     */
    public function searchArticles(PostRepository $postRepo, Request $request)
    {
        $key = $request->get("search");
        $results = $postRepo->findByName($key);
        return $this->render("search-result.html.twig", [
            "results" => $results,
            "key" => $key
        ]);
    }
    /**
     * @Route("/admin")
     */
    public function admin()
    {
        return new Response('<html><body>Admin page!</body></html>');
    }
    /**
     * @Route("/admin/article/{id}/remove", name="remove_article")
     */
    public function removePost(int $id, PostRepository $repo)
    {
        $repo->remove($repo->findById($id));
        return $this->redirectToRoute("blog_index", []);
    }
}
