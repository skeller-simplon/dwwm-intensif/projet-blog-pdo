<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\BlogComment;
use App\Form\BlogCommentType;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\CommentRepository;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PostRepository;


class CommentController extends AbstractController
{
    /**
    * @Route ("/article/{id}/add-comment", name="add_comment")
    */
    public function addComment(int $id = null, Request $request, CommentRepository $commentRepository, PostRepository $postRepository)
    {
        if ($id == null){
            return $this->redirectToRoute('home');
        }
        $comment = new BlogComment();
        $form = $this->createForm(BlogCommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            dump($comment);
            $comment->idBlogPost = $id;
            dump($comment);
            $commentRepository->addComment($comment);
            return $this->redirectToRoute("display_article", [
                'id' => $comment->idBlogPost
            ]);
        }
        return $this->render("add-comment.html.twig", [
            "form" => $form->createView(),
            "article" => $postRepository->findById($id)
        ]);
    }
}
