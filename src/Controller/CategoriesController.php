<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\CategoriesRepository;
use App\Repository\PostRepository;
use Symfony\Component\Routing\Annotation\Route;

class CategoriesController extends AbstractController{
    public function index(CategoriesRepository $repo){
        return $this->render("Ressources/leftBar.html.twig", [
            "categories" => $repo->findAll()
        ]);
    }
    /**
     * @Route ("/sort-id={id}", name="sort_blogposts")
     */
    public function sortById(int $id, PostRepository $repo, CategoriesRepository $catRepo){
        $blogPosts = $repo->findAllByCategoryId($id);
        $category = $catRepo->findById($id);
        return $this->render("sort-blogposts.html.twig", [
            "blogPosts" => $blogPosts,
            "category" => $category
        ]);

    }
    
}