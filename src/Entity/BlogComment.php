<?php

namespace App\Entity;

class BlogComment{
    public $idBlogComment;
    public $content;
    public $creationDate;
    public $author;
    public $idBlogPost;
}