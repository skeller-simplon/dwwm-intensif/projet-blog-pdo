<?php

namespace App\Entity;

class BlogPost{
    public $idBlogPost;
    public $title;
    public $content;
    public $author;
    public $creationDate;
    public $idCategories;
    public $imgUrl;
    public $lastModified;
}