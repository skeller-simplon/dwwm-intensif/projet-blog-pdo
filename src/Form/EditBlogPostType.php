<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\BlogPost;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class EditBlogPostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('id', IntegerType::class)
            // ->setAction($this->generateUrl('display_article'), array('id' => $this->));
            ->add('title', TextType::class, [
                'label' => 'Modifier titre'
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Modifier contenu'
            ])
            ->add('author', TextType::class, [
                'label' => 'Modifier auteur'
            ])
            ->add('idCategories', ChoiceType::class, [
                'choices'  => [
                    'Romans' => 1,
                    'Sérieux' => 2,
                    'Découverte' => 3,
                ],
                'label' => 'Modifier catégorie'
            ])
            ->add('imgUrl', UrlType::class, [
                'label' => 'Modifier image (URL) - Optionnel',
                'required' => false,
                'attr' => [
                    'placeholder' => 'https://journeyman.io/wp-content/uploads/2018/04/placeholder-image-header.jpg'
                ]
            ]);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BlogPost::class,
        ]);
    }
}
