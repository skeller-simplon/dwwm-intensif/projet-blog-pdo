# Exercice : Blog PHP

Dans le cadre d'une formation de Develloppeur Fulstack à Simplon Villeurbanne, ce projet a pour but de faire un blog avec Symfony 4.2 et pdo/Mariadb en deux semaines.

L'application est responsive est utilise surtout Boostrap et Font-Awesome pour la partie design.



## Utilisation

Pour utiliser le projet, il suffit de créer le .env.local avec vos données de BDD.

## Difficultés

Le faire de devoir faire chacun des controllers à la main à posé quelques difficultées au début mais est sans problème à la fin.

Le design est quelque chose pour lequel j'ai encore beaucoup à apprendre mais le résultat est tout à faire correct. 

## Avec plus de temps

Le fait de ne pas utiliser d'ORM rend difficile rapidement la navigation dans les dossiers mal rangés, avec plus de temps ranger les templates et les controllers ne serait pas de trop.

## Copyright

Tout mon code peut-être utilisé, modifié et ré-utilisé sans aucune contrainte.
